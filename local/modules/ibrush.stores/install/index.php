<?
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

IncludeModuleLangFile(__FILE__);

if (class_exists("ibrush_stores"))
	return;

class ibrush_stores extends CModule
{
	var $MODULE_ID = "ibrush.stores";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;


	function __construct()
	{
		$arModuleVersion = array();

		include(dirname(__FILE__)."/version.php");

		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];

		$this->MODULE_NAME = Loc::getMessage("IBRUSH_STORES_INSTALL_NAME");
		$this->MODULE_DESCRIPTION = Loc::getMessage("IBRUSH_STORES_INSTALL_DESCRIPTION");
        $this->PARTNER_NAME = Loc::getMessage('IBRUSH_STORES_VENDOR_NAME');
        $this->PARTNER_URI = Loc::getMessage('IBRUSH_STORES_VENDOR_URL');
	}


	function DoInstall()
	{
        ModuleManager::registerModule($this->MODULE_ID);
	}

    function DoUninstall()
	{
        ModuleManager::unRegisterModule($this->MODULE_ID);
	}
}