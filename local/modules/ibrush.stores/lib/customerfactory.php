<?
namespace Ibrush\Stores;

class CustomerFactory
{

    /**
     * Factory pattern.
     * Method returns instance of WholeSaleCustomer or RetailCustomer class.
     *
     * @param $name
     * @param $productId
     *
     * @return bool
     */
    public static function createCustomer($name, $productId)
    {
        $className = '\\Ibrush\Stores\\' . $name . 'Customer';

        if(class_exists($className))
        {
            return new $className($productId);
        }
        else
        {
            return false;
        }
    }

    /**
     * Method returns current selected city id.
     *
     * @return bool|int
     */
    public static function getCustomerCityId()
    {
        global $APPLICATION;

        if(empty($APPLICATION->get_cookie("REASPEKT_GEOBASE")))
        {
            return false;
        }

        $cityId = intval(json_decode($APPLICATION->get_cookie("REASPEKT_GEOBASE"))->ID);

        if($cityId <= 0)
        {
            return false;
        }

        return $cityId;
    }

    /**
     * Is customer from city with our stores?
     *
     * @return bool
     */
    public static function isCustomerFromOurCity()
    {
        $ourCustomer = false;

        $customerCityId = self::getCustomerCityId();
        $arCitiesIdWhereOurStores = Store::getStoresCitiesIds();

        if($customerCityId == false || $arCitiesIdWhereOurStores == false)
        {
            return $ourCustomer;
        }

        if(in_array($customerCityId, $arCitiesIdWhereOurStores))
        {
            $ourCustomer = true;
        }

        return $ourCustomer;
    }

    /**
     * Check is product in stock in selected city.
     *
     * @param $arProductCitiesAmount - array result from Amount::getCitiesStoresAmountArray
     * @param $customerCityId
     *
     * @return bool
     */
    public static function isProductPresentInCustomerCity($arProductCitiesAmount, $customerCityId)
    {
        return $arProductCitiesAmount[$customerCityId] > 0 ? true : false;
    }

}