<?
namespace Ibrush\Stores;

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Loader;
use \Bitrix\Catalog\StoreProductTable;

Loc::loadMessages(__FILE__);

class Amount
{
    /* Message keys from language file. */
    const IBRUSH_STORES_DELIVERY     = 'IBRUSH_STORES_DELIVERY';
    const IBRUSH_STORES_IN_STOCK     = 'IBRUSH_STORES_IN_STOCK';
    const IBRUSH_STORES_OUT_OF_STOCK = 'IBRUSH_STORES_OUT_OF_STOCK';

    /**
     * Main method for getting stores amount string by productId
     *
     * @param $productId
     *
     * @return bool|string
     */
    public static function getStoresAmountByProductId($productId)
    {
        global $USER;

        $messageKeyStoresAmount = self::IBRUSH_STORES_OUT_OF_STOCK;

        $productId = intval($productId);
        if($productId <= 0)
        {
            return Loc::getMessage($messageKeyStoresAmount);
        }

        /* Who is our client. */
        $customerType = CustomerType::getCustomerType($USER->GetId());

        if($customerType === false)
        {
            /* Our client is nobody. */
            return Loc::getMessage($messageKeyStoresAmount);
        }

        /* Initiate new customer object according his type. */
        $customerObject = CustomerFactory::createCustomer($customerType, $productId);

        if($customerObject === false)
        {
            return Loc::getMessage($messageKeyStoresAmount);
        }

        /* Getting string for message or false */
        $messageKeyStoresAmount = $customerObject->getStoresAmount();

        if($messageKeyStoresAmount === false)
        {
            return false;
        }

        return Loc::getMessage($messageKeyStoresAmount);
    }

    /**
     * Method returns overall stores amount for $productId.
     *
     * @param $productId
     *
     * @return float
     */
    public static function getSummaryProductAmount($productId)
    {
        $productAmount = (float)0;

        if(!Loader::includeModule('catalog'))
        {
            return $productAmount;
        }

        $dbProductResult = StoreProductTable::getList([
            'order'  => ['ID' => 'DESC'],
            'select' => ['AMOUNT'],
            'filter' => [
                '=PRODUCT_ID' => $productId
            ],
        ]);

        while($arAmount = $dbProductResult->fetch())
        {
            $amount = floatval($arAmount['AMOUNT']);

            if($amount > 0)
            {
                $productAmount += $amount;
            }
        }

        return $productAmount;
    }

    /**
     * Return array where key is cityId and value is stores amount for product.
     *
     * @param $productId
     *
     * @return array
     */
    public static function getCitiesStoresAmountArray($productId)
    {
        $arProductAmount = [];

        if(!Loader::includeModule('catalog'))
        {
            return $arProductAmount;
        }

        $dbProductResult = StoreProductTable::getList([
            'order'  => ['ID' => 'DESC'],
            'select' => ['AMOUNT', 'STORE'],
            'filter' => [
                '=PRODUCT_ID' => $productId
            ],
        ]);

        while($arAmount = $dbProductResult->fetch())
        {
            $arProductAmount[$arAmount['CATALOG_STORE_PRODUCT_STORE_XML_ID']] = floatval($arAmount['AMOUNT']);
        }

        return $arProductAmount;
    }

    /**
     * Method returns true if product presents in stock in region.
     *
     * @param $arProductCitiesAmount - array result from getCitiesStoresAmountArray
     * @param $customerCityId
     *
     * @return bool
     */
    public static function isProductPresentNearby($arProductCitiesAmount, $customerCityId)
    {
        /* Detect is customer from Moscow region. */
        if(in_array($customerCityId, Store::$arStores['MOSCOW_REGION']))
        {
            foreach(Store::$arStores['MOSCOW_REGION'] as $item)
            {
                /* Skip current selected city cause we should know there is no stock.  */
                if($item == $customerCityId)
                {
                    continue;
                }

                if($arProductCitiesAmount[$item] > 0)
                {
                    return true;
                }
            }
        }

        /* Detect is customer from Ulyanovsk region. */
        if(in_array($customerCityId, Store::$arStores['ULYANOVSK_REGION']))
        {
            foreach(Store::$arStores['ULYANOVSK_REGION'] as $item)
            {
                /* Skip current selected city cause we should know there is no stock.  */
                if($item == $customerCityId)
                {
                    continue;
                }

                if($arProductCitiesAmount[$item] > 0)
                {
                    return true;
                }
            }
        }

        return false;
    }
}