<?
namespace Ibrush\Stores;

class RetailCustomer extends CustomerType
{
    private $customerCityId;
    private $productId;

    public function __construct($productId)
    {
        $this->customerCityId = CustomerFactory::getCustomerCityId();
        $this->productId = $productId;
    }

    /**
     * Method returns string with stores amount sense.
     *
     * @return bool|string
     */
    public function getStoresAmount()
    {
        if($this->customerCityId === false || $this->productId <= 0)
        {
            return false;
        }

        /* Check store in customer city. */
        if(CustomerFactory::isCustomerFromOurCity() === false)
        {
            return false;
        }

        /* Fill array with stock in cities. */
        $arProductCitiesAmount = Amount::getCitiesStoresAmountArray($this->productId);
        if(empty($arProductCitiesAmount))
        {
            return false;
        }

        /* Detect is product in stock in selected city. */
        if(CustomerFactory::isProductPresentInCustomerCity($arProductCitiesAmount, $this->customerCityId) === true)
        {
            return Amount::IBRUSH_STORES_IN_STOCK;
        }

        return false;
    }


}