<?
namespace Ibrush\Stores;

use \Bitrix\Catalog\StoreTable;
use \Bitrix\Main\Loader;

class Store
{
    /* Cities IDs for regions */
    public static $arStores = [
        'MOSCOW_REGION'    => [760, 803, 599],
        'ULYANOVSK_REGION' => [907, 967, 1041]
    ];

    /**
     * Method returns array with cities ID where our storages are.
     *
     * @return array|bool
     */
    public static function getStoresCitiesIds()
    {
        $arCities = false;

        if(!Loader::includeModule('catalog'))
        {
            return $arCities;
        }

        $dbStoresResult = StoreTable::getList([
            'order'  => ['ID' => 'DESC'],
            'select' => ['XML_ID'],
            'filter' => [
                '=ACTIVE'  => 'Y',
                '=SITE_ID' => SITE_ID,
            ],
        ]);

        while($arCity = $dbStoresResult->fetch())
        {
            $cityId = intval($arCity['XML_ID']);

            if($cityId > 0)
            {
                $arCities[] = $cityId;
            }
        }

        return $arCities;
    }


}