<?
namespace Ibrush\Stores;

class WholeSaleCustomer extends CustomerType
{
    private $customerCityId;
    private $productId;

    public function __construct($productId)
    {
        $this->customerCityId = CustomerFactory::getCustomerCityId();
        $this->productId = $productId;
    }

    /**
     * Method returns string with stores amount sense.
     *
     * @return string
     */
    public function getStoresAmount()
    {
        if($this->customerCityId === false || $this->productId <= 0)
        {
            return Amount::IBRUSH_STORES_OUT_OF_STOCK;
        }

        /* Check store in customer city. */
        if(CustomerFactory::isCustomerFromOurCity() === false)
        {
            return Amount::IBRUSH_STORES_OUT_OF_STOCK;
        }

        /* Check all stores amount for product. */
        if(Amount::getSummaryProductAmount($this->productId) <= 0)
        {
            return Amount::IBRUSH_STORES_OUT_OF_STOCK;
        }

        /* Fill array with all stock in cities. */
        $arProductCitiesAmount = Amount::getCitiesStoresAmountArray($this->productId);
        if(empty($arProductCitiesAmount))
        {
            return Amount::IBRUSH_STORES_OUT_OF_STOCK;
        }

        /* Product presents in selected city. */
        if(CustomerFactory::isProductPresentInCustomerCity($arProductCitiesAmount, $this->customerCityId) === true)
        {
            return Amount::IBRUSH_STORES_IN_STOCK;
        }

        /* Product don't presents in selected city but presents in other region city. */
        if(CustomerFactory::isProductPresentInCustomerCity($arProductCitiesAmount, $this->customerCityId) === false
            && Amount::isProductPresentNearby($arProductCitiesAmount, $this->customerCityId) === true)
        {
            return Amount::IBRUSH_STORES_DELIVERY;
        }

        return Amount::IBRUSH_STORES_OUT_OF_STOCK;
    }


}