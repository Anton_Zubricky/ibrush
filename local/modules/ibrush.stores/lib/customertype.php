<?
namespace Ibrush\Stores;

abstract class CustomerType
{
    const IBRUSH_STORES_RETAIL_CUSTOMERS_GROUP_ID = 10;

    const IBRUSH_STORES_WHOLESALES_CUSTOMERS_GROUP_ID = 9;


    public function getStoresAmount()
    {
    }

    /**
     * Detect what kind of customer is user.
     *
     * @param $userId
     *
     * @return bool|string
     */
    public static function getCustomerType($userId)
    {
        $customerType = false;

        $userId = intval($userId);
        if($userId <= 0)
        {
            return $customerType;
        }

        /* Is customer in two groups simultaneously or not in two groups.  */
        if((self::customerIsRetail($userId) && self::customerIsWholeSales($userId))
            || (!self::customerIsRetail($userId) && !self::customerIsWholeSales($userId)))
        {
            return $customerType;
        }

        if(self::customerIsWholeSales($userId))
        {
            $customerType = 'WholeSale';
        }
        else
        {
            $customerType = 'Retail';
        }

        return $customerType;
    }

    /**
     * Method returns true if user is retail customer.
     *
     * @param $userId
     *
     * @return bool
     */
    private static function customerIsRetail($userId)
    {
        if(in_array(self::IBRUSH_STORES_RETAIL_CUSTOMERS_GROUP_ID, \CUser::GetUserGroup($userId)))
        {
            return true;
        }

        return false;
    }

    /**
     * Method returns true if user is whole sales customer.
     *
     * @param $userId
     *
     * @return bool
     */
    private static function customerIsWholeSales($userId)
    {
        if(in_array(self::IBRUSH_STORES_WHOLESALES_CUSTOMERS_GROUP_ID, \CUser::GetUserGroup($userId)))
        {
            return true;
        }

        return false;
    }


}